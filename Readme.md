# Demo log

## Instructions

Follow the instructions in Demo.md.

To move around steps simply execute following commands choosing steps from range [1-8]:

```shell
> git reset --hard && git clean -f
> git checkout 1 && npm i
```

## Progress summary

- micro demo
  - auto-install
  - external API call with fetch API
  - VSCode tasks
  - testing with AVA
    - resolved db LOCK
  - watching (for Live development and TDD)
  - running in Docker
    - setting custom PORT
  - cluster mode
  - tried PM2

## Tips

Node.js cluster mode allows better CPU utilization with least amount of overhead (yes, container = overhead).

Next steps could be:

- 1 container, 1 node process
- 1 container, several clustered node workers
- several containers, each with several node workers

## TODO

- Kibana monitoring
  - Application activty / usage
  - Machines statistics ([node-agent](https://github.com/dbuarque/node-agent/blob/master/src/index.js) or [express-status-monitor](https://github.com/RafalWilinski/express-status-monitor/blob/master/src/helpers/send-metrics.js))
- CI / CD

## Ideas

- Going through [courses](https://github.com/nodezoo/nodezoo-workshop) or [[2](https://github.com/nearform/micro-services-tutorial-iot)]
- Testing different [boilerplates](https://github.com/Abdizriel/nodejs-microservice-starter)
- SQL Server on Docker
- RabbitMQ [on Ubuntu](https://sub.watchmecode.net/episode/rabbitmq-ubuntu/) with [Node.js](https://sub.watchmecode.net/guides/microservices-with-rabbitmq/)
  - or NATS with [Hemera](https://hemerajs.github.io/hemera-site/)
- Choosing framework:
  - Restify | Express | Koa | Hapi | [Hydra](https://community.risingstack.com/tutorial-building-expressjs-based-microservices-using-hydra/)