const micro = require('micro')
const url = require('url')
const level = require('level')
const promisify = require('then-levelup')
const fetch = require('node-fetch')
const cs = require('callsite-record')
const log4js = require('log4js')

log4js.configure({
  appenders: [
    { type: 'file', filename: 'logs/global.log', category: 'global' }
  ]
})

const globalLogger = log4js.getLogger('global');
globalLogger.setLevel('INFO')

const log = (err) => {
    const error2Log = cs({ forError: err })
    console.log(error2Log.renderSync())
    globalLogger.error(err.message + '\n' + error2Log.renderSync({ renderer: cs.renderers.noColor }))
}

let main = async (request, response) => {
    // console.profile('main');
    const { pathname } = url.parse(request.url)

    switch (pathname) {
        case '/api': {
            let githubStatusResponse;
            try {
                githubStatusResponse = await fetch('https://status.github.com/api/status.json')
                if (githubStatusResponse.status != 200) {
                    const { status: code, statusText } = githubStatusResponse;
                    let err = new Error(`Unexpected resonse from '${githubStatusResponse.url}'`)
                    err.details = { 
                        code,
                        statusText,
                        content: await githubStatusResponse.text(),
                        timestamp: new Date()
                    }
                    throw err;
                }

                micro.send(response, 200, {
                    status: 'success',
                    message: `Response from 'status.github.com/api/status.json'`,
                    data: await githubStatusResponse.json(),
                    process: process.pid
                })
                // console.profileEnd('main');
            } catch (err) {
                log(err)
                micro.send(response, 500, {
                    status: 'failure',
                    error: err.message,
                    details: err.details,
                    process: process.pid
                })
            } break;
        }
        default: {
            micro.send(response, 200, {
                status: 'disabled'
            })
        }
    }
}

let safeMain = (request, response) => {
    try { 
        main(request, response); 
    }
    catch (err) {
        log(err)
    }
}

module.exports = safeMain;