const cluster = require('cluster')
const os = require('os')
const micro = require('../node_modules/micro/lib/index')

if (cluster.isMaster) {

    const cpus = os.cpus().length
    console.log(`Forking for ${cpus} CPUs`)

    for (let i = 0; i < cpus; i++) {
        cluster.fork()
    }
} else {
    micro('./index.js', { port: 3000 }, require('./index.js'))
}