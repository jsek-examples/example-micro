const micro = require('micro')
const test = require('ava')
const listen = require('test-listen')
const request = require('request-promise')

process.env.DBNAME = 'test'

test('index', async t => {
  const service = micro(require('./index'))

  const url = await listen(service)
  const body = await request(url)

  const {status, message} = JSON.parse(body)
  t.deepEqual({status}, {status: 'success'}, 'Should not fail')
  t.regex(message, /[0-9]+ visits/, 'Should return message with number of visits')
})